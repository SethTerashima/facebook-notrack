This is a Google Chrome browser extension that prevents Facebook from monitoring what Newsfeed links you click on. (Note that as a consequence, no "Related Stories" will appear when you visit such a link.) This extension is intended for the privacy conscious.

To install:

1. Navigate to chrome://extensions/ (or click on Tools > Extensions)

2. Click "Load unpacked extension..."

3. Select the folder where you downloaded the files

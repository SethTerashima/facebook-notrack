console.log("Blocking related content AJAX requests...");

chrome.webRequest.onBeforeRequest.addListener(
	function(detail) { return {cancel: true}; },
	{urls: [
		"*://www.facebook.com/pubcontent/related_share/*",
		"*://www.facebook.com/pubcontent/trending/hovercard/logging*"
	]},
	["blocking"]
);




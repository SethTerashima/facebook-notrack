function overwriteFBTracker() {
	if (typeof LinkshimAsyncLink === 'undefined') {
		console.log("Could not find object LinkshimAsyncLink object.");
	} else {
		LinkshimAsyncLink.referrer_log = function (x,y,z) {};
		LinkshimAsyncLink.swap = function(link,url) {link.href = url;};
	};

	// The LinkshimAsyncLink functions are sometimes restored when
	// the user refreshes the page, because for some reason this
	// doesn't re-trigger the script. So we periodically call this
	// function.
	setTimeout(overwriteFBTracker, 3000);
};

overwriteFBTracker();
